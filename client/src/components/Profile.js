import React, { Component } from 'react'
import jwt_decode from "jwt-decode"



class Profile extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             first_name: "",
             last_name: "",
             email:"",
             errors:{}
        }   
    }
    
    componentDidMount(){
        const token=localStorage.usertoken
        const decoded= jwt_decode(token)
        console.log(decoded)
        this.setState({
            first_name: decoded.first_name,
            last_name: decoded.last_name,
            email: decoded.email
        })
    }

    render() {
        return (
            <div>
                <h1>Profile</h1>
                <h4>{this.state.first_name}</h4>
                <h4>{this.state.last_name}</h4>
                <h4>{this.state.email}</h4>
            </div>
        )
    }
}

export default Profile
