import React, { Component } from 'react'
import {Link, withRouter} from 'react-router-dom'


class Landing extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
        this.logOut=this.logOut.bind(this)
    }
    
    logOut(e){
        e.preventDefault()
        localStorage.removeItem('usertoken')
        this.props.history.push('/')

    }
    render() {
        
        return (
            <div>
                <ul>
                    <li>
                        <Link to="/" >
                            Home
                        </Link>
                    </li>
                </ul>
                {localStorage.usertoken ? 
                <div>
                    <ul>
                        <li>
                            <Link to="/profile" >
                                User
                            </Link>
                        </li>
                        <li>
                            <a href="" onClick={this.logOut}>
                                Logout
                            </a>
                        </li>
                    </ul>
                </div>
                :
                <div>
                    <ul>
                        <li>
                            <Link to="/login" >
                                Login
                            </Link>
                        </li>
                        <li>
                            <Link to="/register">
                                Register
                            </Link>
                        </li>
                    </ul>
                </div>}
            </div>
        )
    }
}

export default withRouter(Landing)

