import React, { Component } from 'react'
import {login} from './UserFunctions'


class Login extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             email: "",
             password: "",
             errors: {}
        }
        this.onChange= this.onChange.bind(this)
        this.onSubmit= this.onSubmit.bind(this)


    }

    onChange(e){
        console.log(e.target)
        this.setState({[e.target.name]: e.target.value })
    }
    onSubmit(e){
        e.preventDefault()

        const user= {
            email: this.state.email,
            password: this.state.password
        }
        console.log(login(user))
        login(user).then(res=> {
            console.log(res)
            if(res){
                console.log("Inside profile")
                this.props.history.push('/profile')
            }
        })
        .catch(err=>{
            console.log(err)
        })   
    }


    
    render() {
        return (
            <div> 
                <div class="field">
                <label class="label">Email</label>
                <div class="control has-icons-left has-icons-right">
                    <input class="input is-danger" type="email" placeholder="Email input"  name="email" onChange={this.onChange}/>
                    <span class="icon is-small is-left">
                    <i class="fas fa-envelope"></i>
                    </span>
                    <span class="icon is-small is-right">
                    <i class="fas fa-exclamation-triangle"></i>
                    </span>
                </div>
                </div>

                <div class="field">
                <p class="control has-icons-left">
                    <input class="input" type="password" placeholder="Password" name="password"  onChange={this.onChange}/>
                    <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                    </span>
                </p>
                </div>
                <div class="field">
                <p class="control">
                    <button class="button is-success" onClick={this.onSubmit}>
                    Login
                    </button>
                </p>
                </div>


            </div>
        )
    }
}

export default Login
