import React, { Component } from 'react'
import {register} from './UserFunctions'


class Register extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             first_name:"",
             last_name:"",
             email:"",
             phone_number:"",
             password:"",
             confirmPassword: "",
             errors: {}
        }
        this.onChange= this.onChange.bind(this)
        this.onSubmit= this.onSubmit.bind(this)
    }
    
    onChange(e){
        console.log(e.target)
        this.setState({[e.target.name]: e.target.value})
    }

    onSubmit(e){
        e.preventDefault()

        const newUser= {
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            phone_number: this.state.phone_number,
            email:this.state.email,
            password: this.state.password
        }
        console.log(newUser)
        if(this.state.password===this.state.confirmPassword){
            register(newUser).then(res=>{
                console.log(res)
                this.props.history.push('/login')
            })
        }else{
            alert("Sorry, try again passwords do not match!!!")
        }
    }

    render() {
        return (
            <div>
                <div class="field">
                <label class="label">FirstName</label>
                <div class="control">
                    <input class="input" type="text" placeholder="First Name" name="first_name" onChange={this.onChange}/>
                </div>
                </div>

                <div class="field">
                <label class="label">LastName</label>
                <div class="control">
                    <input class="input" type="text" placeholder="Last Name" name="last_name" onChange={this.onChange}/>
                </div>
                </div>

                <div class="field">
                <label class="label">PhoneNumber</label>
                <div class="control">
                    <input class="input" type="text" placeholder="Phone Number" name="phone_number" onChange={this.onChange}/>
                </div>
                </div>

                <div class="field">
                <label class="label">Email</label>
                <div class="control has-icons-left has-icons-right">
                    <input class="input is-danger" type="email" placeholder="Email input" name="email" onChange={this.onChange}/>
                    <span class="icon is-small is-left">
                    <i class="fas fa-envelope"></i>
                    </span>
                    <span class="icon is-small is-right">
                    <i class="fas fa-exclamation-triangle"></i>
                    </span>
                </div>
                <p class="help is-danger">This email is invalid</p>
                </div>

                <div class="field">
                <p class="control has-icons-left">
                    <input class="input" type="password" placeholder="Password" name="password" onChange={this.onChange}/>
                    <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                    </span>
                </p>
                </div>

                <div class="field">
                <p class="control has-icons-left">
                    <input class="input" type="password" placeholder="ConfirmPassword" name="confirmPassword"  onChange={this.onChange}/>
                    <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
                    </span>
                </p>
                </div>

                <div class="field">
                <p class="control">
                    <button class="button is-success" onClick={this.onSubmit}>
                    Signup
                    </button>
                </p>
                </div>

            </div>
        )
    }
}

export default Register
