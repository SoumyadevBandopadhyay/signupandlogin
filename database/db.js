require('dotenv-extended').load()//.load(options)
const config=require("../config.js")
const Sequelize= require("sequelize")
const db={}

const sequelize= new Sequelize(config.mysql.database, config.mysql.username,config.mysql.password,{
    dialect: 'mysql',
    host: config.mysql.host,
    
    dialectOptions: {
        connectTimeout: config.mysql.timeout,
    },
    pool: {
        max: config.mysql.pool.max,
        min: config.mysql.pool.min,
        acquire: config.mysql.pool.acquire,
        idle : config.mysql.pool.idle
    },
})

db.sequelize= sequelize
db.Sequelize=Sequelize

module.exports= db
